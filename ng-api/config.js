// 项目配置信息
module.exports = {
    server: {
        port: '9999',
        ip: '0.0.0.0'
    },
    db: {
        port: '3306',
        user: 'root',
        password: 'root',
        database: 'ppshop',
        multipleStatements: true
    },
    md5_key: 'fdsa0-sdf--=sf'
}