const express = require('express')
const app = express()
const config = require('./config')
const cors = require('cors')
app.use(cors());
// 引入bodyParser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json({}));
app.use('static', express.static('./public'));
app.use('/api/v1',require('./router'))

// 启动服务器.js
app.listen(config.server.port, config.server.ip, () => console.log(`http://127.0.0.1:${config.server.port}`))