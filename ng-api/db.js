// 引入配置文件
const { db } = require('./config');
// 引入mysql
const mysql = require('mysql');
const connection = mysql.createConnection(db);
connection.connect();
module.exports = connection;